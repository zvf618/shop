# shop

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run servemaster
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint555
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
