import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import './plugins/element.js'
import './assets/css/global.css'    //导入公共样式
import './assets/fonts/iconfont.css'
import TreeTable from 'vue-table-with-tree-grid'
Vue.component('tree-table', TreeTable)

Vue.config.productionTip = false
Vue.prototype.$axios=axios
axios.defaults.baseURL="http://127.0.0.1:8888/api/private/v1/"

//路由守卫
router.beforeEach((to,form,next)=>{
  if(to.path=='/login'){
    next();
    return;
  }else{
    if(!sessionStorage.getItem('token')){
      next('/login');
      return;
    }else{
      next();
    }
  }
});
//请求拦截
axios.interceptors.request.use(config=>{
  config.headers.Authorization=sessionStorage.getItem('token');      //设置请求头
  return config;
})




new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
