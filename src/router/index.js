import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Home from '../views/Home'
import Welcome from '../components/Welcome'
import Users from '../components/Users'
import Rights from '../components/Rights'
import Roles from '../components/Roles'
Vue.use(VueRouter)
//解决重复引入路由报错
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
};

const routes = [{
  path: '/',
  redirect: '/login'
},
{
  path: '/login',
  name: 'Login',
  component: ()=>import('../views/Login.vue')
},
{
  path: '/home',
  name: 'Home',
  redirect: '/welcome',
  component: Home, children: [{
    path: '/welcome', name: Welcome, component: ()=>import('../components/Welcome')
  },
  {
    path: '/users', name: Users, component: ()=>import('../components/Users')
  },
  {
    path: '/rights', name: Rights, component: ()=>import('../components/Rights')
  },
  {
    path: '/roles', name: Roles, component: ()=>import('../components/Roles')
  }
  ,
  {
    path: '/goods', name: Roles, component: ()=>import('../components/ShangPin/List')
  },
  {
    path: '/params', name: Roles, component: ()=>import('../components/ShangPin/Chanshu')
  },
  {
    path: '/categories', name: Roles, component: ()=>import('../components/ShangPin/Fenlei')
  }]
}

]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
